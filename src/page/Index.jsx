import React from 'react';
import { Container, makeStyles } from '@material-ui/core';
import Header from '../layout/Header';
import bg from '../assets/1872.jpg';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 0,
        backgroundImage: "url(" + bg + ")",
        backgroundSize: 'cover'
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));

function Index() {
    const classes = useStyles();

    return (
        <div className={classes.root} >
            <Container style={{backgroundColor: 'rgba(255,255,255,0.7)', filter: 'alpha(opacity=50)',  minHeight: '100vh', width: '100%'}}>
                <Header />
            </Container>
        </div>
    )
}

export default Index