import React from 'react'
import { Typography, Grid, Avatar, Paper, Tabs, Tab, makeStyles, Box } from '@material-ui/core';
import { PropTypes } from 'prop-types';
import { deepOrange } from '@material-ui/core/colors';
import passPhoto from '../assets/pic.jpg'
import PersonalInformation from './PersonalInformation';
import Skill from './Skill';
import ContactMe from './ContactMe';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        backgroundColor: "transparent",
    },
    avatar: {
        color: theme.palette.getContrastText(deepOrange[500]),
        backgroundColor: deepOrange[500],
        marginTop: '5%',
        height: '200px',
        width: '200px'
    },
    paperTitle: {
        background: 'transparent'
    },
    tabPanelTypography: {
        overflow:'auto'
    }
}));


function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
        <Typography
            component="div"
            role="tabpanel"
            style={{overflowY: 'auto', position: 'relative'}}
            hidden={value !== index}
            id={`scrollable-force-tabpanel-${index}`}
            aria-labelledby={`scrollable-force-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
        );
    }

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};


function tabProps(index) {
    return {
        id: `scrollable-force-tab-${index}`,
        'aria-controls': `scrollable-force-tabpanel-${index}`,
    };
}



function Header() {

    const classes = useStyles();

    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const setPaddingDiv = () => {
        let width = window.innerWidth;
        
        if(width < 800) {
            return {
                paddingTop:''
            }
        } else {
            return {
                paddingTop:'180px'
            }
        }
    }

    const setPaddingGrid = () => {
        let width = window.innerWidth;
        
        if(width < 800) {
            return {
                postion:'relative'
            }
        } else {
            return {
                postion:'relative', height: '200px'
            }
        }
    }

    return (
        <div style={{width: '100%'}}>
            <Grid className={classes.paper} container>
                <Grid item sm={12} xs={12} lg={3} xl={3}>
                    <Avatar alt="My Pic" className={classes.avatar} src={passPhoto} />
                </Grid>
                <Grid item lg={7} xl={7} sm={12} xs={12} style={setPaddingGrid()}>
                    <div style={setPaddingDiv()}>
                        <Paper elevation={0} className={classes.paperTitle} square style={{position: 'relative', verticalAlign:'bottom'}}>
                            <Tabs
                                value={value}
                                variant="scrollable"
                                indicatorColor="primary"
                                textColor="primary"
                                aria-label="scrollable force tabs example"
                                onChange={handleChange}
                            >
                                <Tab label="Personal Information" {...tabProps(0)} />
                                <Tab label="Skill and Ability" {...tabProps(1)} />
                                <Tab label="Contact Me" {...tabProps(2)} />
                            </Tabs>
                        </Paper>
                    </div>
                </Grid>
            </Grid>
            <Grid className={classes.paper} container  style={{ overflowY:'auto'}}>
                <Grid item lg={12}>
                    <div>
                        <TabPanel value={value} index={0}>
                            <PersonalInformation />
                        </TabPanel>
                        <TabPanel value={value} index={1}>
                            <Skill />
                        </TabPanel>
                        <TabPanel value={value} index={2}>
                            <ContactMe />
                        </TabPanel>
                    </div>
                </Grid>
            </Grid>
        </div>
    )
}

export default Header;