import React, {useState} from 'react'
import { TextField, Paper, Button } from "@material-ui/core"
import { makeStyles } from '@material-ui/core';
import { Send, Delete } from "@material-ui/icons";
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import { isNull } from 'util';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        padding: '10px',
        backgroundColor: 'inherit'
    },
    divForm: {
        margin: '10px',
        textAlign: 'left'
    },
    textLabel: {
        width: '100%'
    },
    textField: {
        width:'100%'
    }
}));

function ContactMe() {
    const classes = useStyles();

    const[name, setName] = useState("");
    const[company, setCompany] = useState("");
    const[email, setEmail] = useState("");
    const[message, setMessage] = useState("");
    const[title, setTitle] = useState("");

    function handleReset(e) {
        document.getElementById("contactMeForm").reset();
    };

    const handleSubmit = async (e, variant) => {
        e.preventDefault();

        let data = {
            name: name,
            sender: email,
            body: message,
            company: company,
            title: title
        };

        console.log(data);

        await fetch('https://simple-expressjs-typescript.herokuapp.com/api/v1/send/',{
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Basic a9ea6fdc07e93eeb9f4c45fba9a77751007c1b812c5f1250c4e00336cae3b5f0323418fc893b5653d4a81c407c57b43fd32d127e5455a4c213352c2e2b29cadf",
            }
        }).then(res => res.json()
        ).then(data => {
            let variant;
            let message;
            if(data.code === 200) {
                variant = "success";
                message = data.message === null || data.message === undefined ? "Data Sent" : data.message;
            } else {
                variant = "error";
                message = data.message === null || data.message === undefined ? "Send failed!" : data.message;
            }
            showNotif(variant, message);
            document.getElementById("contactMeForm").reset();
        }).catch(err => {
            console.log(err);
            showNotif("error", err);
        });
    }

    const showNotif = (variant, message) => {
        message = isNull(message) ? "" : message;
        switch (variant) {
            case 'success':
                NotificationManager.success(message, 'Success', 2500,() => {return;}, true);
                break;
            case 'error':
                NotificationManager.error(message, 'Click me!', 5000, () => {
                    return;
                }, true);
                break;
            case 'info':
                NotificationManager.info('', '');
                break;
            case 'warning':
                NotificationManager.warning('', '');
                break;
            default:
                break;
        }
    }

    return (
        <div>
            <h2>Contact Me</h2>
            <form noValidate autoComplete="off" id="contactMeForm" onSubmit={(e) => handleSubmit(e, "success")}>
                <div className={classes.divForm}>
                    <Paper className={classes.root} elevation={0}>
                        <label className={classes.textLabel}>Your Name</label>
                        <TextField
                            fullWidth
                            id="name"
                            variant="filled"
                            placeholder="ex: John Arthur"
                            onChange={(e) => {
                                setName(e.target.value)
                            }}
                        />
                    </Paper>
                    <Paper className={classes.root} elevation={0}>
                        <label className={classes.textLabel}>Company Name</label>
                        <TextField
                            fullWidth
                            id="company"
                            variant="filled"
                            placeholder="ex: Liverpool"
                            onChange={(e) => {
                                setCompany(e.target.value)
                            }}
                        />
                    </Paper>
                    <Paper className={classes.root} elevation={0}>
                        <label className={classes.textLabel}>Email Address</label>
                        <TextField
                            fullWidth
                            id="company"
                            variant="filled"
                            placeholder="ex: youremailID@emailCorp.co.uk"
                            onChange={(e) => {
                                setEmail(e.target.value)
                            }}
                        />
                    </Paper>
                    <Paper className={classes.root} elevation={0}>
                        <label className={classes.textLabel}>Message Title</label>
                        <TextField
                            fullWidth
                            id="company"
                            variant="filled"
                            placeholder="ex: Evening Party Invitation"
                            onChange={(e) => {
                                setTitle(e.target.value)
                            }}
                        />
                    </Paper>
                    <Paper className={classes.root} elevation={0}>
                        <label className={classes.textLabel}>Your Messages to Me:</label>
                        <TextField
                            fullWidth
                            id="company"
                            variant="filled"
                            placeholder="ex: Hello, let's talk, I'm on starback on next Saturday at 7pm D:"
                            onChange={(e) => {
                                setMessage(e.target.value)
                            }}
                        />
                    </Paper>
                </div>
                <div>
                    <Button 
                        style={{margin: 30}} 
                        variant="contained" 
                        color="secondary"
                        onClick={(e) =>handleReset(e)}
                    >
                        <Delete fontSize="small" /> Reset
                    </Button>
                    <Button 
                        variant="contained" 
                        color="primary"
                        type="submit"
                    >
                        <Send fontSize="small" />&nbsp;&nbsp;Send Email
                    </Button>
                    <NotificationContainer/>
                </div>
            </form>
        </div>
    )
}

export default ContactMe;