import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Container } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    table: {
        width: '100%'
    },
    trTable: {
        textAlign: 'left'
    },
    td1: {
        width: '20%'
    },
    td2: {
        width: '3%'
    },
    td3: {
        width: '65%'
    },
    divExp: {
        textAlign: 'left'
    },
    spanLi: {

    }
}));

const personalInformationData = [{
    id: 0,
    field: "Name",
    value: "Aji Kuspriambodo"
},{
    id: 1,
    field: "Email Address",
    value: "ajikuspriambodo1993@gmail.com"
},{
    id: 2,
    field: "Last Education",
    value: "Bachelor Degree - STMIK Raharja, Tangerang"
},{
    id: 3,
    field: "GPA",
    value: "3.30/4.00"
}];

const expData = [{
    id: 0,
    title: "Software Engineer, PT Xsis Mitra Utama (August 2018 - now)",
    value: 
        <p>
            Java software Engineer, Assisting team as a Java backend engineer.
            Develop Java web based application with using Spring MVC/Boot and&nbsp;
            Using Hibernate for Data transaction.
            Using javascript, jQuery for Frontend Technology and bootstrap as UI framework.
            And for data transfer using REST concept between UI and backend layer.
        </p>
},{
    id: 1,
    title: "Administrator Mobile, PT Cipar Sukses Bersama (November 2017 - May 2018)",
    value: 
        <p>
            As a representative my corporation that focus in human resources and recruitment. 
            And my placement is at one of Internet Service and TV Cable company in Indonesia at PT Eka Mas Republik (MyRep) Indonesia. 
            My tasks as admin are collaborated with HC team in client company, they are:<br />
            <span>&emsp;1. assisting in filling employee absence</span><br />
            <span>&emsp;2. assisting in creating resign letter for employee</span><br />
            <span>&emsp;3. assisting in making assurance (BPJS Kesehatan, BPJS Ketenagakerjaan, BPJS Jaminan Pensiun)</span><br />
            <span>&emsp;4. assisting in documentation of new recruits</span><br />
            <span>&emsp;5. assisting HC team in documents or letters' distributions (resign, Mutasi, new contract)</span><br />
            <span>&emsp;6. assisting in overcoming employee's rights, as following BPJS, assurance, medical, reimbursement, etc.</span><br />
            <span>&emsp;7. coordinating with Branch Support Supervisor (BSS) and HR department for collecting employee's administrations needed.</span><br />
        </p>
}]

const projectData = [{
    id: 0,
    title: "Fullstack Developer, PT Indoalliz Perkasa Sukses, (November 2018 - May 2019)",
    value:
        <p>
            Assisting IT developer team, for business and operations internal project, most assisting in front-end web, by using Vue.js and Angular 6. 
            Also, assisting in back-end by using golang / beego framework. 
            Building front-web based on RESTful back-end.
        </p>
},{
    id: 1,
    title: "Java Fullstack Developer, PT Mitra Mandiri Informatika, (July 2019 - now)",
    value:
        <p>
            Assisting IT core team to develop web-based application for e-procurement system for one of state-owned enterprises in Indonesia. 
            Develop with Java 7 and Spring mvc with SOAP API concept.
            Using MySql for database and Hibernate framework to do transaction to database.
            Using css, javascript and jquery to design UI.
        </p>
},{
    id: 2,
    title: "Mobile Application Developer, PT Bank BTPN Tbk., (April 2020 - now)",
    value:
        <p>
            Developing mobile application for sales purnabakti team, which the product is loan mobile application for sales team. 
            Using java for mobile and backend. Using Outsystems for database transactions. 
        </p>
}]

const publicationData = [{
    id: 0,
    title: "Problem-Based Learning Mata Kuliah Business Intelligence Pada Perguruan Tinggi Raharja",
    link: "https://widuri.raharja.info/index.php?title=SI1212473436"
},{
    id:1,
    title: "Perancangan Sistem Basis Data Star Schema Prestasi Belajar Siswa Pada SMAN 2 Kota Tangerang",
    link: "http://ejournal.raharja.ac.id/index.php/icit/article/view/16"
}]
function PersonalInformation() {

    const classes = useStyles();

    return (
        <div>
            <Container>
                <h2>
                    Personal Information
                </h2>
                <table className={classes.table} >
                    <tbody>
                        {
                            personalInformationData.map(items => {
                                return(
                                    <tr className={classes.trTable} key={items.id}>
                                        <td className={classes.td1}>{items.field}</td>
                                        <td className={classes.td2}>:</td>
                                        <td className={classes.td3}>{items.value}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                <h2>
                    Experience
                </h2>
                {
                    expData.map(items => {
                        return (
                            <div className={classes.divExp} key={items.id}>
                                <h4>{items.title}</h4>
                                {items.value}
                            </div>
                        )
                    })
                }
                <h2>
                    Project
                </h2>
                {
                    projectData.map(items => {
                        return (
                            <div className={classes.divExp} key={items.id}>
                                <h4>{items.title}</h4>
                                {items.value}
                            </div>
                        )
                    })
                }
                <h2>
                    Publication
                </h2>
                {
                    publicationData.map(items => {
                        return (
                            <div className={classes.divExp} key={items.id}>
                                <h4 style={{textAlign: 'center'}}>
                                    {(items.id + 1)}.&nbsp;
                                    <a href={items.link} rel="noopener noreferrer" target="_blank">{items.title}</a>
                                </h4>
                            </div>
                        )
                    })
                }
            </Container>
        </div>
    )
}

export default PersonalInformation