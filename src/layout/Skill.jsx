import React from 'react'
import { Container, makeStyles } from '@material-ui/core';

const programmingLanguageData = [{
    id: 0,
    title: "Java",
    value: `
        Develop web-based application with Java and Spring MVC/Boot framework. 
        Knowing concept of REST/SOAP Api with Spring.
        Using Hibernate.
        And using Spring mvc with JSP (Java Servlet Page) as alternative web application.
    `
},{
    id: 1,
    title: "Javascript",
    value: `
        Enable to develop web-based with Javascript, whether it uses for Backend or Frontend.
        Using React.js, Angular 4^, and Vue.js to design web UI.
        Using Express or KoaJs to develop backend or services REST application.
        Understand using typescript.
    `
},{
    id: 2,
    title: "Golang",
    value: `
        Using golang to build web application REST services with Beego or Echo.
    `
}]

const foreignLangData = [{
    id: 0,
    title: "English",
    value: "Can use and understand english in conversational or a bit business level."
},{
    id: 1,
    title: "Japanese(日本語)",
    value: "Can use japanese in conversational level and granted by NAT-Test N4 certification."
}]

const useStyle = makeStyles(theme => ({
    prgSkillData: {
        textAlign: "left"
    }
}))

function Skill() {
    const classes = useStyle();

    return (
        <div>
            <Container>
                <h2>
                    Programming Language
                </h2>
                {
                    programmingLanguageData.map(items => {
                        return (
                            <div key={items.id} className={classes.prgSkillData}>
                                <h4>{items.title}</h4>
                                <span>{items.value}</span>
                            </div>
                        )
                    })
                }
                <h2>
                    Foreign Language
                </h2>
                {
                    foreignLangData.map(items => {
                        return (
                            <div key={items.id} className={classes.prgSkillData}>
                                <h4>{items.title}</h4>
                                <p>{items.value}</p>
                            </div>
                        )
                    })
                }
            </Container>
        </div>
    )
}

export default Skill